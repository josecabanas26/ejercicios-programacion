package beca_ejer;

import java.util.Scanner;

public class sumas {
/*pedir un numero al usuario y calcular la suma consecutiva hasta 1 */
	
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		System.out.println("ingresa un numero: ");
		
		int n1 = entrada.nextInt(); 
		int total = 0; 
		
		for (int i=1; i <= n1; i++) {
			total =total+i; 
		}	
		System.out.println("La suma consecutiva de " + n1 + " es: " + total);
	}	
}
